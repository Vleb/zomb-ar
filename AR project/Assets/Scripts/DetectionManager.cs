﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class DetectionManager : MonoBehaviour
{
    [SerializeField] public ARPointCloudManager arPointCloudManager;
    [SerializeField] public GameObject debugSphere;
    [SerializeField] public Transform sphereHolder;
    [SerializeField] public RectTransform cadre;
    [SerializeField] public Camera arCamera;
    [SerializeField] public ARReferencePointManager arReferencePointManager;
    [SerializeField] public GameObject prefab;

    public float maxDistanceBetweenPoint = 0.01f;

    int state = 1;
    Vector3 avgPos;
    TrackableCollection<ARPointCloud> list;
    List<Vector3> exisitingPoints;

    ARReferencePoint referencePoint;

    private void Start()
    {
        avgPos = Vector3.zero;
        list = arPointCloudManager.trackables;
        exisitingPoints = new List<Vector3>();
        prefab.SetActive(false);
    }

    private void Update()
    {
        if (state == 1)
        {
            ProcessPointClouds();
            if (Input.GetMouseButtonUp(0))
            {
                state = 2;
                cadre.gameObject.SetActive(false);
                avgPos /= exisitingPoints.Count;
                InstantiateObj();
            }
        }
    }

    private void InstantiateObj()
    {
        
        Pose ps = new Pose(avgPos, Quaternion.identity);
        referencePoint = arReferencePointManager.AddReferencePoint(ps);
        prefab.SetActive(true);
        prefab.transform.position = ps.position;
        //prefab.transform.localScale = 200 * Vector3.one;
        prefab.transform.parent = null;
        sphereHolder.gameObject.SetActive(false);   

    }

    private void ProcessPointClouds()
    {
        foreach(ARPointCloud point in list)
        {
            for(int i = 0; i < point.positions.Length; ++i)
            {
                Vector3 pos = point.positions[i];
                if (CheckDistance(pos) == true && PointIsOnUIField(pos) == true)
                {
                    exisitingPoints.Add(pos);
                    avgPos += pos;
                    Instantiate(debugSphere, pos, Quaternion.identity, sphereHolder);
                }
            }
        }
    }

    private bool PointIsOnUIField(Vector3 pos)
    {
        Vector2 screenPoint = arCamera.WorldToScreenPoint(pos);
        return RectTransformUtility.RectangleContainsScreenPoint(cadre, screenPoint, null);
    }

    bool CheckDistance(Vector3 pos)
    {
        foreach(Vector3 point in exisitingPoints)
        {
            if (Vector3.Distance(pos, point) < maxDistanceBetweenPoint)
            {
                return false;
            }
        }
        return true;
    }
}
