﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Zombie : MonoBehaviour
{

    [SerializeField] private float distanceToKill = 0.3f;

    GameObject player;

    public void GetHit()
    {
        Destroy(this.gameObject);
    }

    private void Awake()
    {
        player = GameObject.Find("Player");
        if (player)
            Debug.Log("player found");
    }

    private void Update()
    {
        if (Vector3.Distance(this.transform.position, player.transform.position) <= distanceToKill)
        {
            SceneManager.LoadScene("LoseScreen");
        } else
        {
            Debug.Log("too far");
        }
    }
}
