﻿Shader "Custom/ShaderMotionBlur" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
    }

    SubShader {
        Pass {
            ZTest Always
            Cull Off
            ZWrite Off
            
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float4x4 _ViewInverse;
            float4x4 _PrevView;
            float _Intensity;
            int _Quality;

            struct appdata {
                float2 uv : TEXCOORD0;
                float4 vertex : POSITION;
            };

            struct v2f {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v) {
                v2f o;
                o.uv = v.uv;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            float4 frag(v2f v) : SV_Target {
                float2 texcoord = v.uv;
                float4 currentPos = float4(texcoord.x * 2.0 - 1.0, (1.0 - texcoord.y) * 2.0 - 1.0, 1.0, 1.0);
                float4 worldPos = mul(currentPos, _ViewInverse);  
                worldPos /= worldPos.w;
                float4 previousPos = mul(worldPos, _PrevView);  
                previousPos /= previousPos.w;
                float2 velocity = (currentPos.xy - previousPos.xy) * (_Intensity / _Quality);
                float4 color = tex2D(_MainTex, texcoord);
                for (int i = 1; i < _Quality; i++) {
                    texcoord += velocity;   
                    color += tex2D(_MainTex, texcoord);   
                }
                return color / _Quality;
            }

            ENDCG
        }
    }
    FallBack "Diffuse"
}
